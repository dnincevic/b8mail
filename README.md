# B8MAIL

B8MAIL is an OSINT (Open-source intelligence) utility that is a fork of [h8mail](https://github.com/khast3x/h8mail) (a reliable and well know OSINT utility). As of now, B8MAIL's usage is the exact same as h8mail's because we haven't implemented any unique features yet. You can view h8mail's usage [here](https://github.com/khast3x/h8mail#tangerine-usage). Stay tuned!